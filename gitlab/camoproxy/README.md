# camoproxy

![Version: 0.3.0](https://img.shields.io/badge/Version-0.3.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart to deploy Camo Proxy (Go Camo)

## Values

<table>
	<thead>
		<th>Key</th>
		<th>Type</th>
		<th>Default</th>
		<th>Description</th>
	</thead>
	<tbody>
		<tr>
			<td id="affinity"><a href="./values.yaml#L83">affinity</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--enabled"><a href="./values.yaml#L156">autoscaling.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--maxReplicas"><a href="./values.yaml#L158">autoscaling.maxReplicas</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`35`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--minReplicas"><a href="./values.yaml#L157">autoscaling.minReplicas</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`3`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--targetCPUUtilizationPercentage"><a href="./values.yaml#L159">autoscaling.targetCPUUtilizationPercentage</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`75`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--targetMemoryUtilizationPercentage"><a href="./values.yaml#L160">autoscaling.targetMemoryUtilizationPercentage</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`80`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="clusterRole--annotations"><a href="./values.yaml#L133">clusterRole.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="clusterRole--bindingNamespace"><a href="./values.yaml#L135">clusterRole.bindingNamespace</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="clusterRole--enabled"><a href="./values.yaml#L132">clusterRole.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="clusterRole--rules"><a href="./values.yaml#L134">clusterRole.rules</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="commonLabels"><a href="./values.yaml#L9">commonLabels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--annotations"><a href="./values.yaml#L50">deployment.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--allowContentAudio"><a href="./values.yaml#L40">deployment.containerArgs.allowContentAudio</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"--allow-content-audio"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--allowContentVideo"><a href="./values.yaml#L38">deployment.containerArgs.allowContentVideo</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"--allow-content-video"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--cspHeader"><a href="./values.yaml#L46">deployment.containerArgs.cspHeader</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"media-src 'self'; script-src 'none'; object-src 'none'"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--listenPort"><a href="./values.yaml#L30">deployment.containerArgs.listenPort</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`8080`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--logJson"><a href="./values.yaml#L42">deployment.containerArgs.logJson</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"--log-json"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--maxRedirects"><a href="./values.yaml#L32">deployment.containerArgs.maxRedirects</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"--max-redirects=3"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--maxSize"><a href="./values.yaml#L36">deployment.containerArgs.maxSize</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"--max-size=10240"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--metrics"><a href="./values.yaml#L27">deployment.containerArgs.metrics</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"--metrics"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--timeout"><a href="./values.yaml#L34">deployment.containerArgs.timeout</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"--timeout=4s"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerArgs--verbose"><a href="./values.yaml#L44">deployment.containerArgs.verbose</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"--verbose"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--containerCmd[0]"><a href="./values.yaml#L24">deployment.containerCmd[0]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/bin/go-camo"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--enabled"><a href="./values.yaml#L13">deployment.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--image--digest"><a href="./values.yaml#L17">deployment.image.digest</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"cb35a666d181166e52ee2d582b65334799ea30fff92c2ac4d5153049c0aae1ef"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--image--pullPolicy"><a href="./values.yaml#L16">deployment.image.pullPolicy</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"IfNotPresent"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--image--repository"><a href="./values.yaml#L15">deployment.image.repository</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"cactus4docker/go-camo"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--image--tag"><a href="./values.yaml#L19">deployment.image.tag</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"v2.4.1"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--imagePullSecrets"><a href="./values.yaml#L22">deployment.imagePullSecrets</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--probes--liveness--path"><a href="./values.yaml#L77">deployment.probes.liveness.path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/healthcheck"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--probes--readiness--path"><a href="./values.yaml#L80">deployment.probes.readiness.path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/healthcheck"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--resources--limits--memory"><a href="./values.yaml#L66">deployment.resources.limits.memory</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"800Mi"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--resources--requests--cpu"><a href="./values.yaml#L68">deployment.resources.requests.cpu</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"350m"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="deployment--resources--requests--memory"><a href="./values.yaml#L69">deployment.resources.requests.memory</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"800Mi"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="fullnameOverride"><a href="./values.yaml#L6">fullnameOverride</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="hmacKeySecret--key"><a href="./values.yaml#L203">hmacKeySecret.key</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"key"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="hmacKeySecret--name"><a href="./values.yaml#L202">hmacKeySecret.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"camoproxy-hmac"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--annotations--"kubernetes--io/ingress--global-static-ip-name""><a href="./values.yaml#L141">ingress.annotations."kubernetes.io/ingress.global-static-ip-name"</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"camoproxy-static-ip"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--enabled"><a href="./values.yaml#L139">ingress.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--hosts[0]--paths[0]--path"><a href="./values.yaml#L146">ingress.hosts[0].paths[0].path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--hosts[0]--paths[0]--pathType"><a href="./values.yaml#L147">ingress.hosts[0].paths[0].pathType</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"Prefix"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--tls"><a href="./values.yaml#L148">ingress.tls</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="managedCertificate--domains[0]"><a href="./values.yaml#L193">managedCertificate.domains[0]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"camoproxy.example.com"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="managedCertificate--enabled"><a href="./values.yaml#L190">managedCertificate.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="managedCertificate--name"><a href="./values.yaml#L191">managedCertificate.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"managed-cert"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="nameOverride"><a href="./values.yaml#L5">nameOverride</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="networkPolicy--enabled"><a href="./values.yaml#L197">networkPolicy.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="networkPolicy--name"><a href="./values.yaml#L198">networkPolicy.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"camo-deny-internal-egress"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="nodeSelector"><a href="./values.yaml#L86">nodeSelector</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podDisruptionBudget--enabled"><a href="./values.yaml#L168">podDisruptionBudget.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podDisruptionBudget--maxUnavailable"><a href="./values.yaml#L170">podDisruptionBudget.maxUnavailable</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="prometheus_monitoring--enabled"><a href="./values.yaml#L120">prometheus_monitoring.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="prometheus_monitoring--relabelings[0]--sourceLabels[0]"><a href="./values.yaml#L126">prometheus_monitoring.relabelings[0].sourceLabels[0]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"__meta_kubernetes_pod_node_name"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="prometheus_monitoring--relabelings[0]--targetLabel"><a href="./values.yaml#L127">prometheus_monitoring.relabelings[0].targetLabel</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"node"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="prometheus_monitoring--scrape_interval"><a href="./values.yaml#L121">prometheus_monitoring.scrape_interval</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"15s"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="prometheus_monitoring--scrape_timeout"><a href="./values.yaml#L122">prometheus_monitoring.scrape_timeout</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"10s"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--enabled"><a href="./values.yaml#L174">service.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--ports[0]--name"><a href="./values.yaml#L186">service.ports[0].name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"camo"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--ports[0]--port"><a href="./values.yaml#L184">service.ports[0].port</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`80`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--ports[0]--protocol"><a href="./values.yaml#L183">service.ports[0].protocol</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"TCP"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--ports[0]--targetPort"><a href="./values.yaml#L185">service.ports[0].targetPort</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`8080`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--type"><a href="./values.yaml#L175">service.type</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ClusterIP"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--annotations"><a href="./values.yaml#L109">serviceAccount.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--create"><a href="./values.yaml#L108">serviceAccount.create</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="topologySpreadConstraints--constraints[0]--maxSkew"><a href="./values.yaml#L94">topologySpreadConstraints.constraints[0].maxSkew</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="topologySpreadConstraints--constraints[0]--topologyKey"><a href="./values.yaml#L95">topologySpreadConstraints.constraints[0].topologyKey</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"kubernetes.io/hostname"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="topologySpreadConstraints--constraints[0]--whenUnsatisfiable"><a href="./values.yaml#L96">topologySpreadConstraints.constraints[0].whenUnsatisfiable</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ScheduleAnyway"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="topologySpreadConstraints--constraints[1]--maxSkew"><a href="./values.yaml#L97">topologySpreadConstraints.constraints[1].maxSkew</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="topologySpreadConstraints--constraints[1]--topologyKey"><a href="./values.yaml#L98">topologySpreadConstraints.constraints[1].topologyKey</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"topology.kubernetes.io/zone"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="topologySpreadConstraints--constraints[1]--whenUnsatisfiable"><a href="./values.yaml#L99">topologySpreadConstraints.constraints[1].whenUnsatisfiable</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ScheduleAnyway"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="topologySpreadConstraints--enabled"><a href="./values.yaml#L92">topologySpreadConstraints.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>

----------------------------------------------
This README is autogenerated via [helm-docs](https://github.com/norwoodj/helm-docs) in CI. Do not update manually!!