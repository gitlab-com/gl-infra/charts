# promlens

![Version: 0.1.2](https://img.shields.io/badge/Version-0.1.2-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

Helm chart to deploy Promlens

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry-1.docker.io/bitnamicharts | common | ~2 |

## Values

<table>
	<thead>
		<th>Key</th>
		<th>Type</th>
		<th>Default</th>
		<th>Description</th>
	</thead>
	<tbody>
		<tr>
			<td id="affinity"><a href="./values.yaml#L170">affinity</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="args"><a href="./values.yaml#L101">args</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--enabled"><a href="./values.yaml#L343">autoscaling.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--maxReplicas"><a href="./values.yaml#L345">autoscaling.maxReplicas</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--minReplicas"><a href="./values.yaml#L344">autoscaling.minReplicas</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--targetCPU"><a href="./values.yaml#L346">autoscaling.targetCPU</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="autoscaling--targetMemory"><a href="./values.yaml#L347">autoscaling.targetMemory</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="clusterDomain"><a href="./values.yaml#L33">clusterDomain</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"cluster.local"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="command"><a href="./values.yaml#L100">command</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="commonAnnotations"><a href="./values.yaml#L42">commonAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="commonLabels"><a href="./values.yaml#L39">commonLabels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="containerPorts--http"><a href="./values.yaml#L232">containerPorts.http</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`8080`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="containerPorts--https"><a href="./values.yaml#L233">containerPorts.https</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="containerSecurityContext--enabled"><a href="./values.yaml#L224">containerSecurityContext.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="containerSecurityContext--runAsNonRoot"><a href="./values.yaml#L226">containerSecurityContext.runAsNonRoot</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="containerSecurityContext--runAsUser"><a href="./values.yaml#L225">containerSecurityContext.runAsUser</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1001`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="customLivenessProbe"><a href="./values.yaml#L331">customLivenessProbe</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="customReadinessProbe"><a href="./values.yaml#L334">customReadinessProbe</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="customStartupProbe"><a href="./values.yaml#L328">customStartupProbe</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="diagnosticMode--args[0]"><a href="./values.yaml#L57">diagnosticMode.args[0]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"infinity"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="diagnosticMode--command[0]"><a href="./values.yaml#L53">diagnosticMode.command[0]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"sleep"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="diagnosticMode--enabled"><a href="./values.yaml#L49">diagnosticMode.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="extraContainerPorts"><a href="./values.yaml#L240">extraContainerPorts</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="extraDeploy"><a href="./values.yaml#L36">extraDeploy</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="extraEnvVars"><a href="./values.yaml#L108">extraEnvVars</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="extraEnvVarsCM"><a href="./values.yaml#L111">extraEnvVarsCM</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="extraEnvVarsSecret"><a href="./values.yaml#L114">extraEnvVarsSecret</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="extraVolumeMounts"><a href="./values.yaml#L353">extraVolumeMounts</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="extraVolumes"><a href="./values.yaml#L350">extraVolumes</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="fullnameOverride"><a href="./values.yaml#L24">fullnameOverride</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="global--imagePullSecrets"><a href="./values.yaml#L15">global.imagePullSecrets</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="global--imageRegistry"><a href="./values.yaml#L10">global.imageRegistry</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="google--backendConfig--config"><a href="./values.yaml#L483">google.backendConfig.config</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="google--backendConfig--enabled"><a href="./values.yaml#L479">google.backendConfig.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="google--backendConfig--name"><a href="./values.yaml#L481">google.backendConfig.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="google--managedCertificate--domains"><a href="./values.yaml#L490">google.managedCertificate.domains</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="google--managedCertificate--enabled"><a href="./values.yaml#L486">google.managedCertificate.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="google--managedCertificate--name"><a href="./values.yaml#L488">google.managedCertificate.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--annotations"><a href="./values.yaml#L610">healthIngress.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--enabled"><a href="./values.yaml#L585">healthIngress.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--extraHosts"><a href="./values.yaml#L623">healthIngress.extraHosts</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--extraPaths"><a href="./values.yaml#L632">healthIngress.extraPaths</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--extraRules"><a href="./values.yaml#L666">healthIngress.extraRules</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--extraTls"><a href="./values.yaml#L641">healthIngress.extraTls</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--hostname"><a href="./values.yaml#L594">healthIngress.hostname</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"example.local"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--ingressClassName"><a href="./values.yaml#L662">healthIngress.ingressClassName</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--path"><a href="./values.yaml#L598">healthIngress.path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--pathType"><a href="./values.yaml#L591">healthIngress.pathType</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ImplementationSpecific"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--secrets"><a href="./values.yaml#L657">healthIngress.secrets</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--selfSigned"><a href="./values.yaml#L588">healthIngress.selfSigned</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="healthIngress--tls"><a href="./values.yaml#L616">healthIngress.tls</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="hostAliases"><a href="./values.yaml#L95">hostAliases</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="hostIPC"><a href="./values.yaml#L176">hostIPC</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="hostNetwork"><a href="./values.yaml#L173">hostNetwork</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--debug"><a href="./values.yaml#L91">image.debug</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--digest"><a href="./values.yaml#L75">image.digest</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--pullPolicy"><a href="./values.yaml#L80">image.pullPolicy</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"IfNotPresent"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--pullSecrets"><a href="./values.yaml#L88">image.pullSecrets</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--registry"><a href="./values.yaml#L72">image.registry</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"docker.io"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--repository"><a href="./values.yaml#L73">image.repository</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"prom/promlens"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--tag"><a href="./values.yaml#L74">image.tag</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"v0.3.0"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--annotations"><a href="./values.yaml#L526">ingress.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--apiVersion"><a href="./values.yaml#L508">ingress.apiVersion</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--enabled"><a href="./values.yaml#L499">ingress.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--extraHosts"><a href="./values.yaml#L543">ingress.extraHosts</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--extraPaths"><a href="./values.yaml#L552">ingress.extraPaths</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--extraRules"><a href="./values.yaml#L579">ingress.extraRules</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--extraTls"><a href="./values.yaml#L560">ingress.extraTls</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--hostname"><a href="./values.yaml#L511">ingress.hostname</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"promlens.local"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--ingressClassName"><a href="./values.yaml#L531">ingress.ingressClassName</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--path"><a href="./values.yaml#L514">ingress.path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--pathType"><a href="./values.yaml#L505">ingress.pathType</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ImplementationSpecific"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--secrets"><a href="./values.yaml#L575">ingress.secrets</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--selfSigned"><a href="./values.yaml#L502">ingress.selfSigned</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--tls"><a href="./values.yaml#L536">ingress.tls</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="initContainers"><a href="./values.yaml#L390">initContainers</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="kubeVersion"><a href="./values.yaml#L30">kubeVersion</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="lifecycleHooks"><a href="./values.yaml#L267">lifecycleHooks</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="livenessProbe--enabled"><a href="./values.yaml#L304">livenessProbe.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="livenessProbe--failureThreshold"><a href="./values.yaml#L308">livenessProbe.failureThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`6`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="livenessProbe--initialDelaySeconds"><a href="./values.yaml#L305">livenessProbe.initialDelaySeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`30`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="livenessProbe--periodSeconds"><a href="./values.yaml#L307">livenessProbe.periodSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`10`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="livenessProbe--successThreshold"><a href="./values.yaml#L309">livenessProbe.successThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="livenessProbe--timeoutSeconds"><a href="./values.yaml#L306">livenessProbe.timeoutSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="nameOverride"><a href="./values.yaml#L21">nameOverride</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="namespaceOverride"><a href="./values.yaml#L27">namespaceOverride</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="nodeAffinityPreset--key"><a href="./values.yaml#L158">nodeAffinityPreset.key</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="nodeAffinityPreset--type"><a href="./values.yaml#L153">nodeAffinityPreset.type</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="nodeAffinityPreset--values"><a href="./values.yaml#L165">nodeAffinityPreset.values</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="nodeSelector"><a href="./values.yaml#L180">nodeSelector</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="pdb--create"><a href="./values.yaml#L397">pdb.create</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="pdb--maxUnavailable"><a href="./values.yaml#L405">pdb.maxUnavailable</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`0`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="pdb--minAvailable"><a href="./values.yaml#L401">pdb.minAvailable</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podAffinityPreset"><a href="./values.yaml#L142">podAffinityPreset</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podAnnotations"><a href="./values.yaml#L138">podAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podAntiAffinityPreset"><a href="./values.yaml#L146">podAntiAffinityPreset</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"soft"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podLabels"><a href="./values.yaml#L134">podLabels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podSecurityContext--enabled"><a href="./values.yaml#L208">podSecurityContext.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podSecurityContext--fsGroup"><a href="./values.yaml#L209">podSecurityContext.fsGroup</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1001`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podSecurityContext--sysctls"><a href="./values.yaml#L216">podSecurityContext.sysctls</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="priorityClassName"><a href="./values.yaml#L187">priorityClassName</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="readinessProbe--enabled"><a href="./values.yaml#L320">readinessProbe.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="readinessProbe--failureThreshold"><a href="./values.yaml#L324">readinessProbe.failureThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`3`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="readinessProbe--initialDelaySeconds"><a href="./values.yaml#L321">readinessProbe.initialDelaySeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="readinessProbe--periodSeconds"><a href="./values.yaml#L323">readinessProbe.periodSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="readinessProbe--successThreshold"><a href="./values.yaml#L325">readinessProbe.successThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="readinessProbe--timeoutSeconds"><a href="./values.yaml#L322">readinessProbe.timeoutSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`3`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="replicaCount"><a href="./values.yaml#L120">replicaCount</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="resources--limits"><a href="./values.yaml#L255">resources.limits</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="resources--requests"><a href="./values.yaml#L260">resources.requests</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="revisionHistoryLimit"><a href="./values.yaml#L123">revisionHistoryLimit</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`10`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="schedulerName"><a href="./values.yaml#L191">schedulerName</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--annotations"><a href="./values.yaml#L469">service.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--clusterIP"><a href="./values.yaml#L439">service.clusterIP</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--externalTrafficPolicy"><a href="./values.yaml#L473">service.externalTrafficPolicy</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"Cluster"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--extraPorts"><a href="./values.yaml#L453">service.extraPorts</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--loadBalancerIP"><a href="./values.yaml#L443">service.loadBalancerIP</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--loadBalancerSourceRanges"><a href="./values.yaml#L450">service.loadBalancerSourceRanges</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--nodePorts--http"><a href="./values.yaml#L426">service.nodePorts.http</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--nodePorts--https"><a href="./values.yaml#L427">service.nodePorts.https</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--ports--http"><a href="./values.yaml#L419">service.ports.http</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`80`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--ports--https"><a href="./values.yaml#L420">service.ports.https</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`443`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--sessionAffinity"><a href="./values.yaml#L458">service.sessionAffinity</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"None"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--sessionAffinityConfig"><a href="./values.yaml#L464">service.sessionAffinityConfig</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--targetPort--http"><a href="./values.yaml#L433">service.targetPort.http</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--targetPort--https"><a href="./values.yaml#L434">service.targetPort.https</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"https"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--type"><a href="./values.yaml#L414">service.type</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"LoadBalancer"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--annotations"><a href="./values.yaml#L367">serviceAccount.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--automountServiceAccountToken"><a href="./values.yaml#L370">serviceAccount.automountServiceAccountToken</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--create"><a href="./values.yaml#L360">serviceAccount.create</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--name"><a href="./values.yaml#L363">serviceAccount.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="sidecarSingleProcessNamespace"><a href="./values.yaml#L386">sidecarSingleProcessNamespace</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="sidecars"><a href="./values.yaml#L381">sidecars</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="startupProbe--enabled"><a href="./values.yaml#L288">startupProbe.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="startupProbe--failureThreshold"><a href="./values.yaml#L292">startupProbe.failureThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`6`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="startupProbe--initialDelaySeconds"><a href="./values.yaml#L289">startupProbe.initialDelaySeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`30`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="startupProbe--periodSeconds"><a href="./values.yaml#L291">startupProbe.periodSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`10`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="startupProbe--successThreshold"><a href="./values.yaml#L293">startupProbe.successThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="startupProbe--timeoutSeconds"><a href="./values.yaml#L290">startupProbe.timeoutSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="terminationGracePeriodSeconds"><a href="./values.yaml#L195">terminationGracePeriodSeconds</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="tolerations"><a href="./values.yaml#L184">tolerations</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="topologySpreadConstraints"><a href="./values.yaml#L200">topologySpreadConstraints</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="updateStrategy--rollingUpdate"><a href="./values.yaml#L130">updateStrategy.rollingUpdate</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="updateStrategy--type"><a href="./values.yaml#L129">updateStrategy.type</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"RollingUpdate"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>

----------------------------------------------
This README is autogenerated via [helm-docs](https://github.com/norwoodj/helm-docs) in CI. Do not update manually!!