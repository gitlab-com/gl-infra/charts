# thanos-sidecar

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![AppVersion: 0.32.5](https://img.shields.io/badge/AppVersion-0.32.5-informational?style=flat-square)

Deploys the thanos.io sidecar as a standalone deployment to work with grafana mimir

## Source Code

* <https://gitlab.com/gitlab-com/gl-infra/charts/thanos-sidecar>

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry-1.docker.io/bitnamicharts | common | 2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| clusterDomain | string | `"cluster.local"` |  |
| commonAnnotations | object | `{}` | annotations to apply to all resources |
| commonLabels | object | `{}` | labels to apply to all resources |
| fullnameOverride | string | `""` | full name override for bitnami common, common.names.fullname parameter |
| global.imagePullSecrets | list | `[]` | global image pull secrets |
| global.imageRegistry | string | `""` | global image registry |
| global.storageClass | string | `""` | global storage class |
| kubeVersion | string | `""` | force kubernetes target version |
| metrics.enabled | bool | `false` | enable prometheus metrics |
| metrics.serviceMonitor.annotations | object | `{}` | annotations for serviceMonitor. |
| metrics.serviceMonitor.enabled | bool | `false` | enable prometheus-operator serviceMonitor object. |
| metrics.serviceMonitor.honorLabels | bool | `false` | honor source metric labels if there is a collision with target labels. |
| metrics.serviceMonitor.interval | string | `""` | scrape intreval. |
| metrics.serviceMonitor.jobLabel | string | `""` | overrides the job label for prometheus. |
| metrics.serviceMonitor.labels | object | `{}` | labels for serviceMonitor. |
| metrics.serviceMonitor.metricRelabelings | list | `[]` | metric_relabel config. |
| metrics.serviceMonitor.namespace | string | `""` | namespace to deploy serviceMonitor. Defaults to release namespace |
| metrics.serviceMonitor.relabelings | list | `[]` | relabeling configs. |
| metrics.serviceMonitor.scrapeTimeout | string | `""` | scrape timeout. |
| metrics.serviceMonitor.selector | object | `{}` | override the serviceMonitor selector. |
| nameOverride | string | `""` | partial name override for the bitnami common common.names.fullname parameter (keeps release name) |
| namespaceOverride | string | `""` | override deployment namespace |
| nginx.config | string | `"worker_processes  5;  ## Default: 1\nerror_log  /dev/stderr;\npid        /tmp/nginx.pid;\nworker_rlimit_nofile 8192;\n\nevents {\n  worker_connections  4096;  ## Default: 1024\n}\n\nhttp {\n  client_body_temp_path /tmp/client_temp;\n  proxy_temp_path       /tmp/proxy_temp_path;\n  fastcgi_temp_path     /tmp/fastcgi_temp;\n  uwsgi_temp_path       /tmp/uwsgi_temp;\n  scgi_temp_path        /tmp/scgi_temp;\n\n  default_type application/octet-stream;\n  log_format   main '$remote_addr - $remote_user [$time_local]  $status '\n        '\"$request\" $body_bytes_sent \"$http_referer\" '\n        '\"$http_user_agent\" \"$http_x_forwarded_for\"';\n  access_log   /dev/stderr  main;\n\n  sendfile     on;\n  tcp_nopush   on;\n  resolver kube-dns.kube-system.svc.cluster.local;\n\n  server {\n    listen 8080;\n\n    # Distributor endpoints\n    location / {\n      proxy_set_header X-Scope-OrgID {{ join \"|\" .Values.nginx.mimirTenants }};\n      proxy_pass      {{ .Values.nginx.mimirURL }}\n    }\n  }\n}"` |  |
| nginx.image.digest | string | `""` |  |
| nginx.image.pullPolicy | string | `""` |  |
| nginx.image.pullSecrets | list | `[]` |  |
| nginx.image.registry | string | `"docker.io"` |  |
| nginx.image.repository | string | `"nginxinc/nginx-unprivileged"` |  |
| nginx.image.tag | string | `"1.25-alpine"` |  |
| nginx.livenessProbe | object | `{"enabled":true,"failureThreshold":6,"initialDelaySeconds":30,"periodSeconds":5,"successThreshold":1,"timeoutSeconds":10}` | liveness probe config https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/ |
| nginx.mimirTenants[0] | string | `"tenant-a"` |  |
| nginx.mimirTenants[1] | string | `"tenant-b"` |  |
| nginx.mimirTenants[2] | string | `"tenant-c"` |  |
| nginx.mimirURL | string | `"http://mimir-gateway.mimir.svc.cluster.local:80$request_uri;"` | mimir endpoint to proxy requests to. |
| nginx.readinessProbe | object | `{"enabled":true,"failureThreshold":3,"initialDelaySeconds":5,"periodSeconds":3,"successThreshold":1,"timeoutSeconds":5}` | readiness probe config https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/ |
| nginx.resources | object | `{}` |  |
| nginx.startupProbe | object | `{"enabled":false,"failureThreshold":15,"initialDelaySeconds":5,"periodSeconds":5,"successThreshold":1,"timeoutSeconds":1}` | startup probe https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/ |
| service.annotations | object | `{}` | annotations for the service |
| service.clusterIP | string | `""` | cluster IP. E.g "None" for headless. |
| service.externalTrafficPolicy | string | `"Cluster"` | service external traffic policy |
| service.extraPorts | list | `[]` | extra ports for the service |
| service.loadBalancerIP | string | `""` | load balancer ip |
| service.loadBalancerSourceRanges | list | `[]` | allow list source ranges for load balancer |
| service.nodePorts | object | `{"grpc":"","http":""}` | node ports to expose |
| service.ports | object | `{"grpc":10901,"http":10902}` | service ports |
| service.sessionAffinity | string | `"None"` | set service session affinity |
| service.sessionAffinityConfig | object | `{}` | custom session affinity config |
| service.type | string | `"ClusterIP"` | service Type |
| serviceAccount.annotations | object | `{}` | annotations for the service account object |
| serviceAccount.automountServiceAccountToken | bool | `true` | Set if service account should mount token |
| serviceAccount.create | bool | `true` | create service account |
| serviceAccount.name | string | `""` | name override for the service account |
| sidecar.affinity | object | `{}` | custom affinity block. Overrides sidecar.podAffinityPreset |
| sidecar.args | list | `[]` | Overrides default container args |
| sidecar.autoscaling | object | `{"enabled":false,"maxReplicas":"","minReplicas":"","targetCPU":"","targetMemory":""}` | HPA configuration https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/ |
| sidecar.command | list | `[]` | Overrides the sidecar container command |
| sidecar.containerSecurityContext | object | `{"enabled":true,"readOnlyRootFilesystem":false,"runAsNonRoot":true,"runAsUser":1001}` | Configure Container Security Context https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-container |
| sidecar.customLivenessProbe | object | `{}` | set a custom liveness probe. Overrides sidecar.livenessProbe |
| sidecar.customReadinessProbe | object | `{}` | set a custom readiness probe. Overrides sidecar.readinessProbe |
| sidecar.customStartupProbe | object | `{}` | set a custom startup provbe. Overrides sidecar.startupProbe |
| sidecar.extraEnvVars | list | `[]` | extra environment variables to add to the pod |
| sidecar.extraEnvVarsCM | string | `""` | name of a configMap to supply extra env vars from. |
| sidecar.extraEnvVarsSecret | string | `""` | name of secret to supply extra env vars from. |
| sidecar.extraFlags | list | `[]` | extra flags to pass to the container args |
| sidecar.extraVolumeMounts | list | `[]` | volume mounts to add to the pod |
| sidecar.extraVolumes | list | `[]` | additional volumes for the pod |
| sidecar.hostAliases | list | `[]` | add host aliases to pod https://kubernetes.io/docs/concepts/services-networking/add-entries-to-pod-etc-hosts-with-host-aliases/ |
| sidecar.image.digest | optional | `""` | image digest |
| sidecar.image.pullPolicy | string | `"IfNotPresent"` | image pull policy http://kubernetes.io/docs/user-guide/images/#pre-pulling-images |
| sidecar.image.pullSecrets | list | `[]` | image pull secrets, these must be created https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/ |
| sidecar.image.registry | string | `"docker.io"` | thanos sidecar image registry |
| sidecar.image.repository | string | `"bitnami/thanos"` | thanos sidecar image repository |
| sidecar.image.tag | string | `"0.32.5-debian-11-r0"` | thanos sidecar tag |
| sidecar.initContainers | list | `[]` | list of init containers to add to the pod. |
| sidecar.livenessProbe | object | `{"enabled":true,"failureThreshold":6,"initialDelaySeconds":30,"periodSeconds":30,"successThreshold":1,"timeoutSeconds":10}` | liveness probe config https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/ |
| sidecar.logFormat | string | `"logfmt"` | log format for the thanos sidecar |
| sidecar.logLevel | string | `"info"` | log level for the thanos sidecar |
| sidecar.nodeAffinityPreset | object | `{"key":"","type":"","values":[]}` | node affinity presets |
| sidecar.nodeSelector | object | `{}` | node selector config |
| sidecar.pdb | object | `{"create":false,"maxUnavailable":"","minAvailable":1}` | pod distruption budget https://kubernetes.io/docs/tasks/run-application/configure-pdb |
| sidecar.podAffinityPreset | string | `""` | sets a pod affinity preset defined by bitnami common. allowed values: `soft` or `hard` |
| sidecar.podAnnotations | object | `{}` | annotations to apply to the sidecar pod |
| sidecar.podAntiAffinityPreset | string | `"soft"` | sets a pod anti-affinity preset defined by bitnami common. allowed values: `soft` or `hard` |
| sidecar.podLabels | object | `{}` | labels to apply to the sidecar pod |
| sidecar.podSecurityContext | object | `{"enabled":true,"fsGroup":1001}` | Configure Pods Security Context https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod |
| sidecar.priorityClassName | string | `""` | pod priority class |
| sidecar.readinessProbe | object | `{"enabled":true,"failureThreshold":6,"initialDelaySeconds":30,"periodSeconds":30,"successThreshold":1,"timeoutSeconds":10}` | readiness probe config https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/ |
| sidecar.replicaCount | int | `1` | replicas for the deployment |
| sidecar.resources | object | `{"limits":{},"requests":{}}` | sidecar resource requests and limits http://kubernetes.io/docs/user-guide/compute-resources/ |
| sidecar.schedulerName | string | `""` | name of k8s scheduler to use. |
| sidecar.sidecars | list | `[]` | list of sidecars to add to the pod |
| sidecar.startupProbe | object | `{"enabled":false,"failureThreshold":15,"initialDelaySeconds":5,"periodSeconds":5,"successThreshold":1,"timeoutSeconds":1}` | startup probe https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/ |
| sidecar.terminationGracePeriodSeconds | string | `""` | pod termination grace period |
| sidecar.tolerations | list | `[]` | sets tolerations on the pod https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| sidecar.topologySpreadConstraints | list | `[]` | topology spread constraints  https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/#spread-constraints-for-pods |
| sidecar.updateStrategy | object | `{"type":"RollingUpdate"}` | configure update strategy https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#update-strategies |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.0](https://github.com/norwoodj/helm-docs/releases/v1.11.0)
