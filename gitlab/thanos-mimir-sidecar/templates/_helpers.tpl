{{/*
Return the proper thanos sidecar image name
*/}}
{{- define "thanos-sidecar.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.sidecar.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper nginx image name
*/}}
{{- define "thanos-sidecar.nginxImage" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.nginx.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "thanos-sidecar.imagePullSecrets" -}}
{{- include "common.images.pullSecrets" (dict "images" (list .Values.sidecar.image) "global" .Values.global) -}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "thanos-sidecar.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "common.names.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Compile all warnings into a single message.
*/}}
{{- define "thanos-sidecar.validateValues" -}}
{{- $messages := list -}}
{{- $messages := append $messages (include "thanos-sidecar.validateValues.foo" .) -}}
{{- $messages := append $messages (include "thanos-sidecar.validateValues.bar" .) -}}
{{- $messages := without $messages "" -}}
{{- $message := join "\n" $messages -}}

{{- if $message -}}
{{-   printf "\nVALUES VALIDATION:\n%s" $message -}}
{{- end -}}
{{- end -}}

