{{/*
pagerduty_exporter labels
*/}}
{{- define "pagerduty_exporter.labels" -}}
{{ include "common.labels.standard" . }}
app.kubernetes.io/component: pagerduty_exporter
{{- if .Values.commonLabels }}
{{ toYaml .Values.commonLabels }}
{{- end }}
{{- end }}

{{/*
pagerduty_exporter matchlabels
*/}}
{{- define "pagerduty_exporter.matchLabels" -}}
{{ include "common.labels.matchLabels" . }}
app.kubernetes.io/component: pagerduty_exporter
{{- end }}
