{{/*
Create the name of the cluster role
*/}}
{{- define "vault-k8s-secrets.roleName" -}}
{{ include "common.names.fullname" . }}-service-accounts-admin
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "vault-k8s-secrets.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "common.names.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end }}
