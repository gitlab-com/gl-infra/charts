# gcr-cleaner

![Version: 0.1.13](https://img.shields.io/badge/Version-0.1.13-informational?style=flat-square)

Helm chart to deploy a Google Artifact Registry repo cleaner

**Homepage:** <https://github.com/GoogleCloudPlatform/gcr-cleaner>

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Gitlab |  | <https://gitlab.com/gitlab-com/gl-infra/charts> |

## Source Code

* <https://gitlab.com/gitlab-com/gl-infra/charts/gitlab/gcr-cleaner>

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry-1.docker.io/bitnamicharts | common | ~2 |

## Values

<table>
	<thead>
		<th>Key</th>
		<th>Type</th>
		<th>Default</th>
		<th>Description</th>
	</thead>
	<tbody>
		<tr>
			<td id="args"><a href="./values.yaml#L23">args</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="commonAnnotations"><a href="./values.yaml#L9">commonAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="commonLabels"><a href="./values.yaml#L8">commonLabels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="containerSecurityContext--enabled"><a href="./values.yaml#L33">containerSecurityContext.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="cronjob--enabled"><a href="./values.yaml#L13">cronjob.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td>Enable CronJob</td>
		</tr>
		<tr>
			<td id="cronjob--historyLimit"><a href="./values.yaml#L17">cronjob.historyLimit</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="cronjob--schedule"><a href="./values.yaml#L16">cronjob.schedule</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"0 8 * * 6"`
</pre>
</div>
			</td>
			<td>Job schedule 8am on Saturdays</td>
		</tr>
		<tr>
			<td id="image--digest"><a href="./values.yaml#L5">image.digest</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--pullPolicy"><a href="./values.yaml#L6">image.pullPolicy</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"IfNotPresent"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--registry"><a href="./values.yaml#L2">image.registry</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"us-docker.pkg.dev"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--repository"><a href="./values.yaml#L3">image.repository</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"gcr-cleaner/gcr-cleaner/gcr-cleaner-cli"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="image--tag"><a href="./values.yaml#L4">image.tag</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"0.11.1"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="log_level"><a href="./values.yaml#L25">log_level</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"info"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podAnnotations"><a href="./values.yaml#L19">podAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podSecurityContext--enabled"><a href="./values.yaml#L30">podSecurityContext.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="resources"><a href="./values.yaml#L27">resources</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--annotations"><a href="./values.yaml#L43">serviceAccount.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>- Additional annotations to be included on the service account</td>
		</tr>
		<tr>
			<td id="serviceAccount--automountServiceAccountToken"><a href="./values.yaml#L41">serviceAccount.automountServiceAccountToken</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td>- Enable/disable auto mounting of service account token</td>
		</tr>
		<tr>
			<td id="serviceAccount--create"><a href="./values.yaml#L37">serviceAccount.create</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td>- Enable/disable service account creation</td>
		</tr>
		<tr>
			<td id="serviceAccount--labels"><a href="./values.yaml#L45">serviceAccount.labels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>- Additional labels to be included on the service account</td>
		</tr>
		<tr>
			<td id="serviceAccount--name"><a href="./values.yaml#L39">serviceAccount.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td>- Name of the service account to create or use</td>
		</tr>
		<tr>
			<td id="tolerations"><a href="./values.yaml#L20">tolerations</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>

----------------------------------------------
This README is autogenerated via [helm-docs](https://github.com/norwoodj/helm-docs) in CI. Do not update manually!!