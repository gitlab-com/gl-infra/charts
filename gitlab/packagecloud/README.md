# packagecloud

![Version: 0.1.5](https://img.shields.io/badge/Version-0.1.5-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 3.0.10](https://img.shields.io/badge/AppVersion-3.0.10-informational?style=flat-square)

packagecloud:enterprise that powers packages.gitlab.com

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| GitLab |  | <https://gitlab.com/gitlab-com/gl-infra/charts> |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry-1.docker.io/bitnamicharts | common | ~2 |

## Values

<table>
	<thead>
		<th>Key</th>
		<th>Type</th>
		<th>Default</th>
		<th>Description</th>
	</thead>
	<tbody>
		<tr>
			<td id="aws--access_key_file"><a href="./values.yaml#L228">aws.access_key_file</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="aws--host"><a href="./values.yaml#L230">aws.host</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"s3-accelerate.amazonaws.com"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="aws--region"><a href="./values.yaml#L231">aws.region</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"us-east-1"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="aws--secret_key_file"><a href="./values.yaml#L229">aws.secret_key_file</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="cloudfront--secret_file"><a href="./values.yaml#L238">cloudfront.secret_file</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="commonAnnotations"><a href="./values.yaml#L14">commonAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="commonLabels"><a href="./values.yaml#L11">commonLabels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="database--password_file"><a href="./values.yaml#L256">database.password_file</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="database--port"><a href="./values.yaml#L257">database.port</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`3306`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="database--rds_ssl"><a href="./values.yaml#L259">database.rds_ssl</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="database--ssl"><a href="./values.yaml#L258">database.ssl</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="database--username_file"><a href="./values.yaml#L255">database.username_file</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="fullnameOverride"><a href="./values.yaml#L8">fullnameOverride</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="gpg--comment"><a href="./values.yaml#L244">gpg.comment</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="gpg--email"><a href="./values.yaml#L245">gpg.email</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="gpg--expire_date"><a href="./values.yaml#L246">gpg.expire_date</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`0`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="gpg--length"><a href="./values.yaml#L247">gpg.length</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`4096`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="gpg--realname"><a href="./values.yaml#L243">gpg.realname</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--annotations"><a href="./values.yaml#L207">ingress.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--enabled"><a href="./values.yaml#L206">ingress.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--hosts[0]--paths[0]--deployment"><a href="./values.yaml#L211">ingress.hosts[0].paths[0].deployment</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"web"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--hosts[0]--paths[0]--path"><a href="./values.yaml#L210">ingress.hosts[0].paths[0].path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/*"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--ssl--enabled"><a href="./values.yaml#L205">ingress.ssl.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--tls"><a href="./values.yaml#L212">ingress.tls</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="managedCertificate--domains"><a href="./values.yaml#L219">managedCertificate.domains</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="managedCertificate--enabled"><a href="./values.yaml#L217">managedCertificate.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="managedCertificate--name"><a href="./values.yaml#L218">managedCertificate.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="nameOverride"><a href="./values.yaml#L5">nameOverride</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--api_token_file"><a href="./values.yaml#L41">packagecloud.api_token_file</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--bootstrapDatabase"><a href="./values.yaml#L148">packagecloud.bootstrapDatabase</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--disabledServices[0]"><a href="./values.yaml#L94">packagecloud.deployments.rainbows.disabledServices[0]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"resque"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--livenessProbe--failureThreshold"><a href="./values.yaml#L104">packagecloud.deployments.rainbows.livenessProbe.failureThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`10`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--livenessProbe--httpGet--path"><a href="./values.yaml#L97">packagecloud.deployments.rainbows.livenessProbe.httpGet.path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--livenessProbe--httpGet--port"><a href="./values.yaml#L98">packagecloud.deployments.rainbows.livenessProbe.httpGet.port</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--livenessProbe--httpGet--scheme"><a href="./values.yaml#L99">packagecloud.deployments.rainbows.livenessProbe.httpGet.scheme</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"HTTP"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--livenessProbe--initialDelaySeconds"><a href="./values.yaml#L100">packagecloud.deployments.rainbows.livenessProbe.initialDelaySeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`45`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--livenessProbe--periodSeconds"><a href="./values.yaml#L101">packagecloud.deployments.rainbows.livenessProbe.periodSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`30`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--livenessProbe--successThreshold"><a href="./values.yaml#L103">packagecloud.deployments.rainbows.livenessProbe.successThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--livenessProbe--timeoutSeconds"><a href="./values.yaml#L102">packagecloud.deployments.rainbows.livenessProbe.timeoutSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--ports[0]--containerPort"><a href="./values.yaml#L86">packagecloud.deployments.rainbows.ports[0].containerPort</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`80`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--ports[0]--name"><a href="./values.yaml#L85">packagecloud.deployments.rainbows.ports[0].name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--readinessProbe--failureThreshold"><a href="./values.yaml#L114">packagecloud.deployments.rainbows.readinessProbe.failureThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`2`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--readinessProbe--httpGet--path"><a href="./values.yaml#L107">packagecloud.deployments.rainbows.readinessProbe.httpGet.path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--readinessProbe--httpGet--port"><a href="./values.yaml#L108">packagecloud.deployments.rainbows.readinessProbe.httpGet.port</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--readinessProbe--httpGet--scheme"><a href="./values.yaml#L109">packagecloud.deployments.rainbows.readinessProbe.httpGet.scheme</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"HTTP"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--readinessProbe--initialDelaySeconds"><a href="./values.yaml#L110">packagecloud.deployments.rainbows.readinessProbe.initialDelaySeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`45`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--readinessProbe--periodSeconds"><a href="./values.yaml#L111">packagecloud.deployments.rainbows.readinessProbe.periodSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--readinessProbe--successThreshold"><a href="./values.yaml#L113">packagecloud.deployments.rainbows.readinessProbe.successThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--readinessProbe--timeoutSeconds"><a href="./values.yaml#L112">packagecloud.deployments.rainbows.readinessProbe.timeoutSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`2`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--service--ports[0]--name"><a href="./values.yaml#L90">packagecloud.deployments.rainbows.service.ports[0].name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--service--ports[0]--port"><a href="./values.yaml#L91">packagecloud.deployments.rainbows.service.ports[0].port</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`80`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--service--ports[0]--targetPort"><a href="./values.yaml#L92">packagecloud.deployments.rainbows.service.ports[0].targetPort</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--service--type"><a href="./values.yaml#L88">packagecloud.deployments.rainbows.service.type</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ClusterIP"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--strategy--rollingUpdate--maxSurge"><a href="./values.yaml#L117">packagecloud.deployments.rainbows.strategy.rollingUpdate.maxSurge</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"100%"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--rainbows--strategy--rollingUpdate--maxUnavailable"><a href="./values.yaml#L118">packagecloud.deployments.rainbows.strategy.rollingUpdate.maxUnavailable</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"0%"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--resque--disabledServices[0]"><a href="./values.yaml#L121">packagecloud.deployments.resque.disabledServices[0]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"rainbows"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--resque--disabledServices[1]"><a href="./values.yaml#L122">packagecloud.deployments.resque.disabledServices[1]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"nginx"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--resque--disabledServices[2]"><a href="./values.yaml#L123">packagecloud.deployments.resque.disabledServices[2]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"unicorn"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--resque--hpa--maxReplicas"><a href="./values.yaml#L126">packagecloud.deployments.resque.hpa.maxReplicas</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--resque--hpa--minReplicas"><a href="./values.yaml#L125">packagecloud.deployments.resque.hpa.minReplicas</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--resque--hpa--targetCPU"><a href="./values.yaml#L127">packagecloud.deployments.resque.hpa.targetCPU</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`70`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--resque--strategy--rollingUpdate--maxSurge"><a href="./values.yaml#L130">packagecloud.deployments.resque.strategy.rollingUpdate.maxSurge</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"100%"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--resque--strategy--rollingUpdate--maxUnavailable"><a href="./values.yaml#L131">packagecloud.deployments.resque.strategy.rollingUpdate.maxUnavailable</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"0%"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--toolbox--disabledServices[0]"><a href="./values.yaml#L134">packagecloud.deployments.toolbox.disabledServices[0]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"nginx"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--toolbox--disabledServices[1]"><a href="./values.yaml#L135">packagecloud.deployments.toolbox.disabledServices[1]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"unicorn"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--toolbox--disabledServices[2]"><a href="./values.yaml#L136">packagecloud.deployments.toolbox.disabledServices[2]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"rainbows"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--toolbox--disabledServices[3]"><a href="./values.yaml#L137">packagecloud.deployments.toolbox.disabledServices[3]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"resque"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--disabledServices[0]"><a href="./values.yaml#L57">packagecloud.deployments.web.disabledServices[0]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"resque"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--disabledServices[1]"><a href="./values.yaml#L58">packagecloud.deployments.web.disabledServices[1]</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"rainbows"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--livenessProbe--failureThreshold"><a href="./values.yaml#L68">packagecloud.deployments.web.livenessProbe.failureThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`10`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--livenessProbe--httpGet--path"><a href="./values.yaml#L61">packagecloud.deployments.web.livenessProbe.httpGet.path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--livenessProbe--httpGet--port"><a href="./values.yaml#L62">packagecloud.deployments.web.livenessProbe.httpGet.port</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--livenessProbe--httpGet--scheme"><a href="./values.yaml#L63">packagecloud.deployments.web.livenessProbe.httpGet.scheme</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"HTTP"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--livenessProbe--initialDelaySeconds"><a href="./values.yaml#L64">packagecloud.deployments.web.livenessProbe.initialDelaySeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`45`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--livenessProbe--periodSeconds"><a href="./values.yaml#L65">packagecloud.deployments.web.livenessProbe.periodSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`30`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--livenessProbe--successThreshold"><a href="./values.yaml#L67">packagecloud.deployments.web.livenessProbe.successThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--livenessProbe--timeoutSeconds"><a href="./values.yaml#L66">packagecloud.deployments.web.livenessProbe.timeoutSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--ports[0]--containerPort"><a href="./values.yaml#L49">packagecloud.deployments.web.ports[0].containerPort</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`80`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--ports[0]--name"><a href="./values.yaml#L48">packagecloud.deployments.web.ports[0].name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--readinessProbe--failureThreshold"><a href="./values.yaml#L78">packagecloud.deployments.web.readinessProbe.failureThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`2`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--readinessProbe--httpGet--path"><a href="./values.yaml#L71">packagecloud.deployments.web.readinessProbe.httpGet.path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--readinessProbe--httpGet--port"><a href="./values.yaml#L72">packagecloud.deployments.web.readinessProbe.httpGet.port</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--readinessProbe--httpGet--scheme"><a href="./values.yaml#L73">packagecloud.deployments.web.readinessProbe.httpGet.scheme</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"HTTP"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--readinessProbe--initialDelaySeconds"><a href="./values.yaml#L74">packagecloud.deployments.web.readinessProbe.initialDelaySeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`45`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--readinessProbe--periodSeconds"><a href="./values.yaml#L75">packagecloud.deployments.web.readinessProbe.periodSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`5`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--readinessProbe--successThreshold"><a href="./values.yaml#L77">packagecloud.deployments.web.readinessProbe.successThreshold</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--readinessProbe--timeoutSeconds"><a href="./values.yaml#L76">packagecloud.deployments.web.readinessProbe.timeoutSeconds</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`2`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--service--ports[0]--name"><a href="./values.yaml#L53">packagecloud.deployments.web.service.ports[0].name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--service--ports[0]--port"><a href="./values.yaml#L54">packagecloud.deployments.web.service.ports[0].port</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`80`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--service--ports[0]--targetPort"><a href="./values.yaml#L55">packagecloud.deployments.web.service.ports[0].targetPort</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--service--type"><a href="./values.yaml#L51">packagecloud.deployments.web.service.type</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ClusterIP"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--strategy--rollingUpdate--maxSurge"><a href="./values.yaml#L81">packagecloud.deployments.web.strategy.rollingUpdate.maxSurge</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"100%"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--deployments--web--strategy--rollingUpdate--maxUnavailable"><a href="./values.yaml#L82">packagecloud.deployments.web.strategy.rollingUpdate.maxUnavailable</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"0%"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--enterprise_license_key_file"><a href="./values.yaml#L37">packagecloud.enterprise_license_key_file</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--external_url"><a href="./values.yaml#L33">packagecloud.external_url</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--extraContainers"><a href="./values.yaml#L199">packagecloud.extraContainers</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--extraVolumeMounts"><a href="./values.yaml#L185">packagecloud.extraVolumeMounts</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--extraVolumes"><a href="./values.yaml#L177">packagecloud.extraVolumes</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--image--pullPolicy"><a href="./values.yaml#L159">packagecloud.image.pullPolicy</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ifNotPresent"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--image--registry"><a href="./values.yaml#L156">packagecloud.image.registry</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"registry.gitlab.com"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--image--repository"><a href="./values.yaml#L157">packagecloud.image.repository</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"gitlab-com/gl-infra/ci-images/packagecloud"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--image--tag"><a href="./values.yaml#L158">packagecloud.image.tag</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"1.2.0"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--podAnnotations"><a href="./values.yaml#L169">packagecloud.podAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--podLabels"><a href="./values.yaml#L164">packagecloud.podLabels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="packagecloud--users"><a href="./values.yaml#L144">packagecloud.users</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="rainbows--worker_processes"><a href="./values.yaml#L289">rainbows.worker_processes</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`2`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="redis--port"><a href="./values.yaml#L266">redis.port</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`6379`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="resque--delete_worker_count"><a href="./values.yaml#L298">resque.delete_worker_count</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="resque--index_worker_count"><a href="./values.yaml#L297">resque.index_worker_count</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`1`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--create"><a href="./values.yaml#L20">serviceAccount.create</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--labels"><a href="./values.yaml#L23">serviceAccount.labels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--name"><a href="./values.yaml#L21">serviceAccount.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`nil`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--nameTest"><a href="./values.yaml#L22">serviceAccount.nameTest</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`nil`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="smtp--password_file"><a href="./values.yaml#L273">smtp.password_file</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="unicorn--worker_processes"><a href="./values.yaml#L281">unicorn.worker_processes</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`2`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="unicorn--worker_timeout"><a href="./values.yaml#L282">unicorn.worker_timeout</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`300`
</pre>
</div>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>

----------------------------------------------
This README is autogenerated via [helm-docs](https://github.com/norwoodj/helm-docs) in CI. Do not update manually!!