{{ $validationPassed := true -}}
CHART NAME: {{ .Chart.Name  }}
CHART VERSION: {{ .Chart.Version  }}
APP VERSION: {{ .Chart.AppVersion  }}

** Please be patient while the chart is being deployed **
{{- if not (regexMatch "^http" .Values.packagecloud.external_url) }}
{{- $validationPassed = false }}

packagecloud: packagecloud.external_url

ERROR: You did not specify a valid external URL. It must be the full URL including the scheme. For example: https://packages.gitlab.com
{{- end -}}
{{- if not .Values.packagecloud.enterprise_license_key_file }}
{{- $validationPassed = false }}

packagecloud: packagecloud.enterprise_license_key_file

ERROR: You did not specify a path to the file that contains the packagecloud:enterprise license key.
{{- end -}}
{{- if not .Values.packagecloud.api_token_file }}
{{- $validationPassed = false }}

packagecloud: packagecloud.api_token_file

ERROR: You did not specify a path to the file that contains the packagecloud.io API token.
{{- end -}}
{{- if not (and .Values.aws.access_key_file .Values.aws.secret_key_file) }}
{{- $validationPassed = false }}

packagecloud: aws.access_key_file, aws.secret_key_file

ERROR: You did not specify the path to the AWS access key file and/or the AWS secret key file.
{{- end -}}
{{- if not .Values.cloudfront.secret_file }}
{{- $validationPassed = false }}

packagecloud: cloudfront.secret_file

ERROR: You did not specify a path to the file that contains the Cloudfront secret.
{{- end -}}
{{- if not (and .Values.database.username_file .Values.database.password_file) -}}
{{- $validationPassed = false }}

packagecloud: database.username_file, database.password_file

ERROR: You did not specify the DB username file and/or the DB user's password file.
{{- end -}}

{{- if $validationPassed }}

######################
# Validation passed! #
######################

The following packagecloud components were deployed:

{{ range $name, $component := omit .Values.components "common" }}
{{- with .deployment }}
{{- if .enabled -}}
- {{ $name }}
{{ end }}
{{- end }}
{{- end }}

{{- end }}
