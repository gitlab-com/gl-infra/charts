{{/*
Finds and returns the first element in a list of dicts that matches
a given .value and, optionally, the .key (defaults to "name")

Usage:
{{ include "packagecloud.utils.getValueByKey" (dict "list" .Values.some-list "value" "value-of-name") }}
*/}}
{{- define "packagecloud.utils.getValueByKey" -}}
  {{- if and .list .value }}
    {{- if .list }}
      {{- $found := false }}
      {{- $key := default "name" .key }}
      {{- if (kindIs "slice" .list) }}
        {{- range .list }}
          {{- if and . (eq false $found) }}
            {{- if eq (get . $key) $.value -}}
              {{- $found = true }}
              {{- toYaml (list .) | indent 0 }}
            {{- end }}
          {{- end }}
        {{- end }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end -}}
