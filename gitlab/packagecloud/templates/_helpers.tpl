{{/* vim: set filetype=mustache: */}}

{{- define "packagecloud.config_rb" -}}
## Url on which packagecloud will be reachable.
external_url '{{ .packagecloud.external_url }}'

## The following settings are mandatory for a working installation:
##
##      1.) packagecloud:enterprise license key
##      2.) GPG keys
##      3.) AWS credentials
##      4.) Mailer settings
##

## License key settings
##
##
## Replace 'your-license-key' with your enterprise license key.
## Replace 'your-api-token' with your packagecloud.io API token (https://packagecloud.io/api_token).
##
## Please contact enterprise@packagecloud.io if you need help retreiving your
## enterprise license key.
##
packagecloud_rails['enterprise_license_key'] = File.read("{{ .packagecloud.enterprise_license_key_file }}").strip
packagecloud_rails['api_token'] = File.read("{{ .packagecloud.api_token_file }}").strip

## GPG key settings
## if you want packagecloud to generate a key for you, skip to the next section.
##
## if you already have a GPG key you'd like to use:
##
##     change the follow settings:
##        generate_key: when set to false this will prevent packagecloud from
##                      generating a key for
##
##        key_directory: set to the directory where the gpg key you want to use
##                       resides
##
##
##
packagecloud_gpg['generate_key'] = false
packagecloud_gpg['key_directory'] = "/etc/packagecloud/gpgkey"

##
## GPG key generation settings
##
## Change these values so they are relevant for your company
##
packagecloud_gpg['realname'] = {{ .gpg.realname | quote }}
packagecloud_gpg['comment'] = {{ .gpg.comment | quote }}
packagecloud_gpg['email'] = {{ .gpg.email | quote }}
packagecloud_gpg['expire_date'] = {{ .gpg.expire_date }}
packagecloud_gpg['length'] = {{ .gpg.length }}

##
## Resque worker count
##
## The more workers, the faster indexing will occur after uploading a
## package, but also more memory will be required for workers to run.
##
## You shouldn't need more than 6 resque workers and many users will be
## fine with just 1.
##
{{- range $k, $v := .packagecloud.resque }}
resque['{{ $k }}'] = {{ $v | quote }}
{{- end }}

## AWS settings
##
##  NOTE: It is recommended that you create an IAM user and specify the exact
##        access policy you want.
##
##  !!! You MUST choose a bucket name and fill in the value below for
##      'aws_repo_bucket_name'
##
##  !!! If you set aws_create_bucket to true please ensure your IAM user has
##      sufficient access rights to create buckets.
##
packagecloud_rails['aws_access_key'] = File.read("{{ .aws.access_key_file }}").strip
packagecloud_rails['aws_secret_key'] = File.read("{{ .aws.secret_key_file }}").strip
packagecloud_rails['aws_create_bucket'] = {{ .aws.create_bucket }}
packagecloud_rails['aws_repo_bucket_name'] = {{ .aws.repo_bucket_name | quote }}
packagecloud_rails['aws_region'] = {{ .aws.region | quote }}
packagecloud_rails['aws_host'] = {{ .aws.host | quote }}

packagecloud_rails['cloudfront_enabled'] = true
packagecloud_rails['cloudfront_secret'] = File.read("{{ .cloudfront.secret_file }}").strip
packagecloud_rails['cloudfront_distribution_id'] = {{ .cloudfront.distribution_id | quote }}
packagecloud_rails['cloudfront_packages_domain'] = {{ .cloudfront.packages_domain | quote }}

packagecloud_rails['user_seed_file'] = '/etc/packagecloud/users.yml'

## Mailer settings
smtp['address'] = {{ .smtp.address | quote }}
smtp['domain'] = {{ .smtp.domain | quote }}
smtp['port'] = {{ .smtp.port }}
smtp['authentication'] = {{ .smtp.authentication | quote }}
smtp['enable_starttls_auto'] = {{ .smtp.enable_starttls_auto }}
{{- if .smtp.from }}
smtp['from'] = {{ .smtp.from | quote }}
{{- end }}
{{- if .smtp.reply_to }}
smtp['reply_to'] = {{ .smtp.reply_to | quote }}
{{- end }}
{{- if .smtp.username }}
smtp['user_name'] = {{ .smtp.username | quote }}
{{- end }}

{{- if .smtp.password_file }}
smtp['password'] = File.read("{{ .smtp.password_file }}").strip
{{- end }}

## Disable embedded MySQL
mysql['enable'] = false

## External DB
packagecloud_rails['database_host'] = {{ .database.host | quote }}
packagecloud_rails['database_port'] = {{ .database.port }}
packagecloud_rails['database_name'] = {{ .database.name | quote }}
packagecloud_rails['database_user'] = File.read("{{ .database.username_file }}").strip
packagecloud_rails['database_password'] = File.read("{{ .database.password_file }}").strip
packagecloud_rails['database_ssl'] = {{ .database.ssl }}
packagecloud_rails['rds_ssl'] = {{ .database.rds_ssl }}

## External Redis
packagecloud_rails['redis_host'] = {{ .redis.host | quote }}
packagecloud_rails['redis_port'] = {{ .redis.port }}

## Backup settings
backups['config_backup'] = false
backups['database_backup'] = false

## SSL Settings
##
## By default, SSL is disabled. Before enabling SSL, you will need to obtain an
## SSL certificate and key.
##
## This option enables SSL
##
# packagecloud_rails['packagecloud_https'] = true
# packagecloud_rails['packagecloud_port'] = 443
##
## The full path to your SSL certificate
##
# nginx['ssl_certificate'] = "/etc/packagecloud/ssl/server.crt"
##
## The full path to your SSL private key
##
# nginx['ssl_certificate_key'] = "/etc/packagecloud/ssl/server.key"
##
## This next option instructs NGINX to redirect plain-text connections to SSL
## automatically.
##
## If using SSL, this option is highly recommended.
##
# nginx['redirect_http_to_https'] = true
# nginx['ssl_protocols'] = "TLSv1.2 TLSv1.3"
# nginx['ssl_ciphers'] = "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384"
nginx['proxy_read_timeout'] = 3600 # 60 minutes
nginx['custom_packagecloud_server_config'] = "location = /nginx_status {
    stub_status on;
    access_log off;
    allow 127.0.0.1;
    deny all;
  }
"
nginx['client_max_body_size'] = "1500"
nginx['listen_addresses'] = %w{* [::]}

## Logrotate Settings
##
## How many logs to keep around
##
## logging['logrotate_rotate'] = 30
##
## Compress rotated logs
##
## logging['logrotate_compress'] = "compress" # see 'man logrotate'
##
## Logrotate method (see 'man logrotate')
##
## logging['logrotate_method'] = "copytruncate" # see 'man logrotate'
##
## Rotate based on frequency, like "daily", or, nil to rotate only on size
##
## logging['logrotate_frequency'] = nil
##
## Rotate based on size
##
## logging['logrotate_size'] = "100M"

{{- range $k, $v := .packagecloud.unicorn }}
unicorn['{{ $k }}'] = {{ $v | quote }}
{{- end }}

{{- range $k, $v := .packagecloud.rainbows }}
rainbows['{{ $k }}'] = {{ $v | quote }}
{{- end }}

{{- end -}}

{{/*
Create the name of the service account
*/}}
{{- define "packagecloud.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "common.names.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
