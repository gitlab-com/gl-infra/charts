{{/*
gitalyctl labels
*/}}
{{- define "gitalyctl.labels" -}}
{{ include "common.labels.standard" . }}
app.kubernetes.io/component: gitalyctl
{{- if .Values.commonLabels }}
{{ toYaml .Values.commonLabels }}
{{- end }}
{{- end }}

{{/*
gitalyctl matchlabels
*/}}
{{- define "gitalyctl.matchLabels" -}}
{{ include "common.labels.matchLabels" . }}
app.kubernetes.io/component: gitalyctl
{{- end }}
