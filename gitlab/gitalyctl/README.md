# gitalyctl

![Version: 0.3.0](https://img.shields.io/badge/Version-0.3.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

Deploy `woodhouse gitalyctl storage drain`, which moves all the git
repositories from the configured Gitaly storage to different Gitaly storage
so that the drained one can be decommissioned.

This is currently being used for [multi-project
migration](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/library/gitaly-multi-project/README.md)

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry-1.docker.io/bitnamicharts | common | ~2 |
| oci://registry.ops.gitlab.net/gitlab-com/gl-infra/charts | vault-secrets | ~1 |

## Values

<table>
	<thead>
		<th>Key</th>
		<th>Type</th>
		<th>Default</th>
		<th>Description</th>
	</thead>
	<tbody>
		<tr>
			<td id="commonAnnotations"><a href="./values.yaml#L6">commonAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Define common annotations to be added to every resource.</td>
		</tr>
		<tr>
			<td id="commonLabels"><a href="./values.yaml#L3">commonLabels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Define common labels to be added to every resource, such as `type`, `env`, `environemnt`, `tier`, and `stage`.</td>
		</tr>
		<tr>
			<td id="gitalyctl"><a href="./values.yaml#L9">gitalyctl</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"config":{},"enabled":false,"image":{"pullPolicy":"Always","registry":"registry.gitlab.com","repository":"gitlab-com/gl-infra/woodhouse","tag":"latest"},"livenessProbe":{"enabled":false},"metrics":{"enabled":false},"readinessProbe":{"enabled":false},"resources":{},"secrets":{"gitalyctl_api_token":{"key":"gitalyctl_api_token","name":"gitalyctl"}}}`
</pre>
</div>
			</td>
			<td>All configuration around the gitalyctl deployment.</td>
		</tr>
		<tr>
			<td id="gitalyctl--config"><a href="./values.yaml#L33">gitalyctl.config</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Configure `gitalyctl`: <https://gitlab.com/gitlab-com/gl-infra/woodhouse/-/blob/master/configs/gitalyctl-storage-drain-config.example.yml></td>
		</tr>
		<tr>
			<td id="gitalyctl--enabled"><a href="./values.yaml#L11">gitalyctl.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td>Enable gitalyctl</td>
		</tr>
		<tr>
			<td id="gitalyctl--image--pullPolicy"><a href="./values.yaml#L20">gitalyctl.image.pullPolicy</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"Always"`
</pre>
</div>
			</td>
			<td>Pull Policy for the image.</td>
		</tr>
		<tr>
			<td id="gitalyctl--image--registry"><a href="./values.yaml#L14">gitalyctl.image.registry</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"registry.gitlab.com"`
</pre>
</div>
			</td>
			<td>Which container registry to use for the image.</td>
		</tr>
		<tr>
			<td id="gitalyctl--image--repository"><a href="./values.yaml#L16">gitalyctl.image.repository</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"gitlab-com/gl-infra/woodhouse"`
</pre>
</div>
			</td>
			<td>Specify the path for the image excluding the container registry.</td>
		</tr>
		<tr>
			<td id="gitalyctl--image--tag"><a href="./values.yaml#L18">gitalyctl.image.tag</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"latest"`
</pre>
</div>
			</td>
			<td>Image tag to use for gitalyctl.</td>
		</tr>
		<tr>
			<td id="gitalyctl--livenessProbe--enabled"><a href="./values.yaml#L26">gitalyctl.livenessProbe.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td>Enable liveness probe</td>
		</tr>
		<tr>
			<td id="gitalyctl--metrics--enabled"><a href="./values.yaml#L23">gitalyctl.metrics.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td>Enable Prometheus metrics endpoint and monitor it via a PodMonitor: <https://github.com/prometheus-operator/prometheus-operator/tree/main#customresourcedefinitions></td>
		</tr>
		<tr>
			<td id="gitalyctl--readinessProbe--enabled"><a href="./values.yaml#L29">gitalyctl.readinessProbe.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td>Enable readiness probe</td>
		</tr>
		<tr>
			<td id="gitalyctl--resources"><a href="./values.yaml#L31">gitalyctl.resources</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Define any resource `requests`/`limits`: <https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/></td>
		</tr>
		<tr>
			<td id="gitalyctl--secrets--gitalyctl_api_token"><a href="./values.yaml#L36">gitalyctl.secrets.gitalyctl_api_token</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"key":"gitalyctl_api_token","name":"gitalyctl"}`
</pre>
</div>
			</td>
			<td>GitLab API admin token used by `woodhouse gitalyctl`, ideally coming from Hashicorp Vault.</td>
		</tr>
		<tr>
			<td id="vault-secrets"><a href="./values.yaml#L42">vault-secrets</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"enabled":false}`
</pre>
</div>
			</td>
			<td>external secrets dependency See: https://gitlab.com/gitlab-com/gl-infra/charts/-/tree/main/gitlab/vault-secrets</td>
		</tr>
		<tr>
			<td id="vault-secrets--enabled"><a href="./values.yaml#L44">vault-secrets.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td>Enable vault-secrets.</td>
		</tr>
	</tbody>
</table>

----------------------------------------------
This README is autogenerated via [helm-docs](https://github.com/norwoodj/helm-docs) in CI. Do not update manually!!