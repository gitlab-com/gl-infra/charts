{{/*
mailgun_exporter labels
*/}}
{{- define "mailgun_exporter.labels" -}}
{{ include "common.labels.standard" . }}
app.kubernetes.io/component: mailgun_exporter
{{- if .Values.commonLabels }}
{{ toYaml .Values.commonLabels }}
{{- end }}
{{- end }}

{{/*
mailgun_exporter matchlabels
*/}}
{{- define "mailgun_exporter.matchLabels" -}}
{{ include "common.labels.matchLabels" . }}
app.kubernetes.io/component: mailgun_exporter
{{- end }}
