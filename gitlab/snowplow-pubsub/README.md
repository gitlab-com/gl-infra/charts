# snowplow-pubsub

![Version: 0.1.15](https://img.shields.io/badge/Version-0.1.15-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart for installing Snowplow using the Google PubSub collector.

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry-1.docker.io/bitnamicharts | common | ~2 |

## Values

<table>
	<thead>
		<th>Key</th>
		<th>Type</th>
		<th>Default</th>
		<th>Description</th>
	</thead>
	<tbody>
		<tr>
			<td id="affinity"><a href="./values.yaml#L281">affinity</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="collector"><a href="./values.yaml#L40">collector</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"autoscaling":{"enabled":true,"maxReplicas":100,"minReplicas":1,"targetCPUUtilizationPercentage":80},"config":"collector {\n  interface = \"0.0.0.0\"\n  port = {{ .Values.service.targetPort }}\n\n  streams {\n    good = \"collector-good\"\n    bad = \"collector-bad\"\n\n    sink {\n      googleProjectId = {{ .Values.googleProjectId | quote }}\n    }\n  }\n}\n","enabled":true,"image":{"pullPolicy":"IfNotPresent","pullSecrets":[],"registry":"docker.io","repository":"snowplow/scala-stream-collector-pubsub","tag":"2.9.2"},"livenessProbe":{"enabled":true,"failureThreshold":3,"initialDelaySeconds":0,"path":"/health","periodSeconds":10,"port":80,"successThreshold":1,"timeoutSeconds":1},"podAnnotations":{},"readinessProbe":{"enabled":true,"failureThreshold":3,"initialDelaySeconds":0,"path":"/health","periodSeconds":10,"port":80,"successThreshold":1,"timeoutSeconds":1},"resources":{}}`
</pre>
</div>
			</td>
			<td>Settings for the Snowplow collector</td>
		</tr>
		<tr>
			<td id="collector--autoscaling"><a href="./values.yaml#L73">collector.autoscaling</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"enabled":true,"maxReplicas":100,"minReplicas":1,"targetCPUUtilizationPercentage":80}`
</pre>
</div>
			</td>
			<td>Autoscaling configuration for Snowplow Collector.</td>
		</tr>
		<tr>
			<td id="collector--config"><a href="./values.yaml#L82">collector.config</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"collector {\n  interface = \"0.0.0.0\"\n  port = {{ .Values.service.targetPort }}\n\n  streams {\n    good = \"collector-good\"\n    bad = \"collector-bad\"\n\n    sink {\n      googleProjectId = {{ .Values.googleProjectId | quote }}\n    }\n  }\n}\n"`
</pre>
</div>
			</td>
			<td>Configuration required for the snowplow collector in HOCON format.</td>
		</tr>
		<tr>
			<td id="collector--enabled"><a href="./values.yaml#L42">collector.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td>Enable the Snowplow Collector</td>
		</tr>
		<tr>
			<td id="collector--image"><a href="./values.yaml#L44">collector.image</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"pullPolicy":"IfNotPresent","pullSecrets":[],"registry":"docker.io","repository":"snowplow/scala-stream-collector-pubsub","tag":"2.9.2"}`
</pre>
</div>
			</td>
			<td>Image to use for Snowplow Collector</td>
		</tr>
		<tr>
			<td id="collector--livenessProbe"><a href="./values.yaml#L61">collector.livenessProbe</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"enabled":true,"failureThreshold":3,"initialDelaySeconds":0,"path":"/health","periodSeconds":10,"port":80,"successThreshold":1,"timeoutSeconds":1}`
</pre>
</div>
			</td>
			<td>Liveness Probe settings for Snowplow collector</td>
		</tr>
		<tr>
			<td id="collector--podAnnotations"><a href="./values.yaml#L80">collector.podAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Annotations on the collector pods</td>
		</tr>
		<tr>
			<td id="collector--readinessProbe"><a href="./values.yaml#L51">collector.readinessProbe</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"enabled":true,"failureThreshold":3,"initialDelaySeconds":0,"path":"/health","periodSeconds":10,"port":80,"successThreshold":1,"timeoutSeconds":1}`
</pre>
</div>
			</td>
			<td>Readiness Probe settings for Snowplow collector</td>
		</tr>
		<tr>
			<td id="collector--resources"><a href="./values.yaml#L71">collector.resources</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Resource requests for the Snowplow Collector.</td>
		</tr>
		<tr>
			<td id="commonAnnotations"><a href="./values.yaml#L15">commonAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Annotations common to all Kubernetes objects</td>
		</tr>
		<tr>
			<td id="commonLabels"><a href="./values.yaml#L13">commonLabels</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Labels common to all Kubernetes objects.</td>
		</tr>
		<tr>
			<td id="enricher"><a href="./values.yaml#L98">enricher</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"autoscaling":{"enabled":true,"maxReplicas":100,"minReplicas":1,"targetCPUUtilizationPercentage":80},"config":"{\n  \"input\": {\n    \"subscription\": \"projects/{{ .Values.googleProjectId }}/subscriptions/collector-good-sub\"\n  }\n\n  \"output\": {\n    \"good\": {\n      \"topic\": \"projects/{{ .Values.googleProjectId }}/topics/enriched\"\n    }\n\n    \"bad\": {\n      \"topic\": \"projects/{{ .Values.googleProjectId }}/topics/enricher-bad\"\n    }\n  }\n}\n","enabled":true,"iglu":"{\n  \"schema\": \"iglu:com.snowplowanalytics.iglu/resolver-config/jsonschema/1-0-3\",\n  \"data\": {\n    \"cacheSize\": 500,\n    \"repositories\": [\n      {\n        \"name\": \"Iglu Central\",\n        \"priority\": 0,\n        \"vendorPrefixes\": [ \"com.snowplowanalytics\" ],\n        \"connection\": {\n          \"http\": {\n            \"uri\": \"http://iglucentral.com\"\n          }\n        }\n      }\n    ]\n  }\n}\n","image":{"pullPolicy":"IfNotPresent","pullSecrets":[],"registry":"docker.io","repository":"snowplow/snowplow-enrich-pubsub","tag":"3.6.0.3"},"podAnnotations":{},"resources":{}}`
</pre>
</div>
			</td>
			<td>Settings for the Snowplow enricher</td>
		</tr>
		<tr>
			<td id="enricher--autoscaling"><a href="./values.yaml#L111">enricher.autoscaling</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"enabled":true,"maxReplicas":100,"minReplicas":1,"targetCPUUtilizationPercentage":80}`
</pre>
</div>
			</td>
			<td>Autoscaling configuration for Snowplow enricher.</td>
		</tr>
		<tr>
			<td id="enricher--config"><a href="./values.yaml#L120">enricher.config</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"{\n  \"input\": {\n    \"subscription\": \"projects/{{ .Values.googleProjectId }}/subscriptions/collector-good-sub\"\n  }\n\n  \"output\": {\n    \"good\": {\n      \"topic\": \"projects/{{ .Values.googleProjectId }}/topics/enriched\"\n    }\n\n    \"bad\": {\n      \"topic\": \"projects/{{ .Values.googleProjectId }}/topics/enricher-bad\"\n    }\n  }\n}\n"`
</pre>
</div>
			</td>
			<td>Configuration required for the snowplow enricher in HOCON format.</td>
		</tr>
		<tr>
			<td id="enricher--enabled"><a href="./values.yaml#L100">enricher.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td>Enable the Snowplow enricher</td>
		</tr>
		<tr>
			<td id="enricher--image"><a href="./values.yaml#L102">enricher.image</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{"pullPolicy":"IfNotPresent","pullSecrets":[],"registry":"docker.io","repository":"snowplow/snowplow-enrich-pubsub","tag":"3.6.0.3"}`
</pre>
</div>
			</td>
			<td>Image for the Snowplow enricher</td>
		</tr>
		<tr>
			<td id="enricher--podAnnotations"><a href="./values.yaml#L118">enricher.podAnnotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Annotations on the enricher pods</td>
		</tr>
		<tr>
			<td id="enricher--resources"><a href="./values.yaml#L109">enricher.resources</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Resource requests for the Snowplow enricher.</td>
		</tr>
		<tr>
			<td id="env"><a href="./values.yaml#L35">env</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td>Environment variables to add to pods</td>
		</tr>
		<tr>
			<td id="fullnameOverride"><a href="./values.yaml#L20">fullnameOverride</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td>Release full name override</td>
		</tr>
		<tr>
			<td id="gcpSecretName"><a href="./values.yaml#L33">gcpSecretName</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td>Existing secret name that contains a valid GCP credentials.json </td>
		</tr>
		<tr>
			<td id="global--imagePullSecrets"><a href="./values.yaml#L10">global.imagePullSecrets</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="global--imageRegistry"><a href="./values.yaml#L5">global.imageRegistry</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="googleProjectId"><a href="./values.yaml#L31">googleProjectId</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"aspiring-alpaca"`
</pre>
</div>
			</td>
			<td>Google project ID where the PubSub topics live</td>
		</tr>
		<tr>
			<td id="ingress--annotations"><a href="./values.yaml#L211">ingress.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--apiVersion"><a href="./values.yaml#L181">ingress.apiVersion</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--enabled"><a href="./values.yaml#L178">ingress.enabled</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--extraHosts"><a href="./values.yaml#L227">ingress.extraHosts</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--extraPaths"><a href="./values.yaml#L236">ingress.extraPaths</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--extraRules"><a href="./values.yaml#L275">ingress.extraRules</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--extraTls"><a href="./values.yaml#L245">ingress.extraTls</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--hostname"><a href="./values.yaml#L189">ingress.hostname</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"snowplow.local"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--ingressClassName"><a href="./values.yaml#L186">ingress.ingressClassName</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--path"><a href="./values.yaml#L192">ingress.path</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"/"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--pathType"><a href="./values.yaml#L195">ingress.pathType</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ImplementationSpecific"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--secrets"><a href="./values.yaml#L261">ingress.secrets</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--servicePort"><a href="./values.yaml#L199">ingress.servicePort</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"http"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="ingress--tls"><a href="./values.yaml#L219">ingress.tls</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`false`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="nameOverride"><a href="./values.yaml#L18">nameOverride</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td>Release name override</td>
		</tr>
		<tr>
			<td id="nodeSelector"><a href="./values.yaml#L277">nodeSelector</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="podSecurityContext"><a href="./values.yaml#L158">podSecurityContext</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="securityContext"><a href="./values.yaml#L161">securityContext</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--annotations"><a href="./values.yaml#L173">service.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--port"><a href="./values.yaml#L171">service.port</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`80`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--targetPort"><a href="./values.yaml#L172">service.targetPort</a></td>
			<td>
int
</td>
			<td>
				<div>
<pre lang="">
`8080`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="service--type"><a href="./values.yaml#L170">service.type</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`"ClusterIP"`
</pre>
</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td id="serviceAccount--annotations"><a href="./values.yaml#L26">serviceAccount.annotations</a></td>
			<td>
object
</td>
			<td>
				<div>
<pre lang="">
`{}`
</pre>
</div>
			</td>
			<td>Annotations on the service account</td>
		</tr>
		<tr>
			<td id="serviceAccount--create"><a href="./values.yaml#L24">serviceAccount.create</a></td>
			<td>
bool
</td>
			<td>
				<div>
<pre lang="">
`true`
</pre>
</div>
			</td>
			<td>Whether to create a service account for this workload</td>
		</tr>
		<tr>
			<td id="serviceAccount--name"><a href="./values.yaml#L28">serviceAccount.name</a></td>
			<td>
string
</td>
			<td>
				<div>
<pre lang="">
`""`
</pre>
</div>
			</td>
			<td>Name of the service account</td>
		</tr>
		<tr>
			<td id="tolerations"><a href="./values.yaml#L279">tolerations</a></td>
			<td>
list
</td>
			<td>
				<div>
<pre lang="">
`[]`
</pre>
</div>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>

----------------------------------------------
This README is autogenerated via [helm-docs](https://github.com/norwoodj/helm-docs) in CI. Do not update manually!!