{{/*
Enricher labels
*/}}
{{- define "snowplow-pubsub.enricher.labels" -}}
{{ include "common.labels.standard" . }}
app.kubernetes.io/component: enricher
{{- end }}

{{/*
Collector labels
*/}}
{{- define "snowplow-pubsub.collector.labels" -}}
{{ include "common.labels.standard" . }}
app.kubernetes.io/component: collector
{{- end }}

{{/*
Enricher labels to use on deploy.spec.selector.matchLabels and svc.spec.selector
*/}}
{{- define "snowplow-pubsub.enricher.matchLabels" -}}
{{ include "common.labels.matchLabels" . }}
app.kubernetes.io/component: enricher
{{- end -}}

{{/*
Collector labels to use on deploy.spec.selector.matchLabels and svc.spec.selector
*/}}
{{- define "snowplow-pubsub.collector.matchLabels" -}}
{{ include "common.labels.matchLabels" . }}
app.kubernetes.io/component: collector
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "snowplow-pubsub.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "common.names.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Collector SVC name
*/}}
{{- define "snowplow-pubsub.collector.serviceName" -}}
{{ include "common.names.fullname" . }}-collector
{{- end }}
