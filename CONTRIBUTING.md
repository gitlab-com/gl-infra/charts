# Contributing

Contributions are welcome via Merge Requests.

## How to Contribute
1. Clone this repository, develop, and test your changes.
2. Push to a new branch
3. Submit a new Merge Request with a description following - `[chartname] summary of change`.This helps to make it clear what chart is effected with your change.   
If your change does not relate to a specific chart you can use `[compoent] summary of change`, for example `[ci/cd] - update ci stage`.  
Changes to multiple charts should be submitted via seperate Merge Requests.  

## Commits and Versioning

Commits messages should follow [conventional commit](https://www.conventionalcommits.org/).  
All changes should be reflected in the `Chart.yaml` versioning via [semver](https://semver.org/) versioning.  

## Bitnami Common

When templating your charts please make use of the [bitnami common](https://github.com/bitnami/charts/tree/main/bitnami/common) library.  
This keeps a baseline set of labels/annotations across all charts and makes usage a lot more streamlined.   
`common.labels.standard` will automatically add a baseline set of labels to the resource, while also adding `.Values.commonLabels` and `.Values.commonAnnotations` will enable easy assignment of common resource labels to all objects when templating.  

Example:
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ $name }}
  labels: {{- include "common.labels.standard" $ | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  {{- if .Values.commonAnnotations }}
  annotations:
    {{- if .Values.commonAnnotations }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
    {{- end }}
```

## Documentation 

All charts require a readme.  
This is automatically generated via CI but requires commenting your `values.yaml`.  
See [here](README.md#documentation) for more information.  

