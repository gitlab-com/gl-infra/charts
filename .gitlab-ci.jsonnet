// Chart Directory
local chartDir = 'gitlab';

// Stages
local stages = {
  stages: [
    'notify',
    'test',
    'diff',
    'validate',
    'release',
    'docs',
  ],
};

// Rules
local ruleProtected = '$CI_COMMIT_REF_PROTECTED == "true"';
local ruleOnDefault = '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH';
local ruleTargetDefault = '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH';
local ruleNotSchedule = '$CI_PIPELINE_SOURCE != "schedule"';
local ruleMergeRequest = '$CI_PIPELINE_SOURCE == "merge_request_event"';
local ruleMergeTargetDefaultBranch = '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH';
local ruleAnd(rule) =
  ' && %s' % rule;
local ruleOr(rule) =
  ' || %s' % rule;
local ruleGitlabCom = '$CI_API_V4_URL == "https://gitlab.com/api/v4"';
local ruleGitlabOps = '$CI_API_V4_URL == "https://ops.gitlab.net/api/v4"';

// Includes
local includes = {
  include: [
    // Dependency scanning
    // https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
    { template: 'Jobs/Dependency-Scanning.gitlab-ci.yml' },
    // Dependency scanning
    // https://docs.gitlab.com/ee/user/application_security/iac_scanning/
    { template: 'Jobs/SAST-IaC.gitlab-ci.yml' },
    // https://gitlab.com/gitlab-com/gl-infra/ci-images/-/tree/master/k8s-workloads
    { 'local': '.gitlab-ci.image.yml' },
  ],
};

// Global Vars
local globalVars = {
  variables: {
    COLOR_DIFF: '\\033[1;36m',
    COLOR_OFF: '\\033[0m',
  },
};

// ID Tokens
local idTokens = {
  '.id_tokens': {
    id_tokens: {
      VAULT_ID_TOKEN: {
        aud: 'https://vault.gitlab.net',
      },
    },
  },
};

// CI Diff
local ciDiff = {
  'ci::diff': {
    extends: '.k8s-workloads-image',
    stage: 'diff',
    needs: [],
    script: |||
      make generate-diff
      diff .gitlab-ci.yml .gitlab-ci-diff.yml && echo "No diff detected." || (printf '\n!!!!!!!!\nDiff detected in .gitlab-ci.yml, please run "make generate" to ensure CI config is up to date\n!!!!!!!!\n'; exit 2)
    |||,
    rules: [
      {
        'if': ruleMergeRequest + ruleAnd(ruleMergeTargetDefaultBranch) + ruleAnd(ruleNotSchedule) + ruleAnd(ruleGitlabCom),
        when: 'always',
      },
    ],
  },
};


local helm = {
  '.helm': {
    extends: '.k8s-workloads-image',
    before_script: |||
      cd ${CHART_PATH}
    |||,
  },
};

// Helm
local helmJobs() =
  {
    [std.split(dir, '/')[1] + '::validate']: {
      stage: 'validate',
      extends: ['.helm', '.id_tokens'],
      needs: [],
      variables: {
        CHART_DIR: chartDir,
        CHART_PATH: dir,
      },
      secrets: {
        REGISTRY_TOKEN: {
          vault: 'access_tokens/ops-gitlab-net/${CI_PROJECT_PATH}/helm-registry-readonly/token@ci',
          file: false,
        },
      },
      script: |||
        echo ${REGISTRY_TOKEN} | helm registry login registry.ops.gitlab.net -u ${CI_REGISTRY_USER} --password-stdin
        helm dependency update .
        printf %b "${COLOR_DIFF}######## Validating: helm lint ########${COLOR_OFF}\n";
        helm lint .
        printf %b "${COLOR_DIFF}######## Validating: helm template ########${COLOR_OFF}\n";
        helm template . > manifests.yaml
        printf %b "${COLOR_DIFF}######## Validating: generated manifests are valid ########${COLOR_OFF}\n";
        kubeconform -summary -ignore-missing-schemas -strict -schema-location default -schema-location 'https://raw.githubusercontent.com/datreeio/CRDs-catalog/main/{{.Group}}/{{.ResourceKind}}_{{.ResourceAPIVersion}}.json' manifests.yaml
      |||,
      rules: [
        {
          'if': ruleGitlabOps,
          when: 'never',
        },
        {
          'if': ruleMergeRequest + ruleAnd(ruleMergeTargetDefaultBranch) + ruleAnd(ruleNotSchedule),
          changes: [
            // Chart Dir
            '%s/**' % dir,
          ],
        },
      ],
    }
    for dir in std.split(std.extVar('dirs'), ',')
  }
  {
    [std.split(dir, '/')[1] + '::release']: {
      stage: 'release',
      extends: ['.helm', '.id_tokens'],
      tags: [
        'gitlab-org',
      ],
      variables: {
        CHART_DIR: chartDir,
        CHART_PATH: dir,
      },
      secrets: {
        REGISTRY_TOKEN: {
          vault: 'access_tokens/${VAULT_SECRETS_PATH}/helm-registry-publish/token@ci',
          file: false,
        },
      },
      script: |||
        echo "${REGISTRY_TOKEN}" | helm registry login ${CI_REGISTRY}/${CI_PROJECT_PATH} -u ${CI_REGISTRY_USER} --password-stdin
        helm dependency update .
        helm package .
        chart_archive="$(find *.tgz)"
        helm push "${chart_archive}" oci://${CI_REGISTRY}/${CI_PROJECT_PATH}
        curl --request POST \
             --user gitlab-ci-token:$CI_JOB_TOKEN \
             --form "chart=@${chart_archive}" \
             "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts"
      |||,
      rules: [
        {
          'if': ruleGitlabCom,
          when: 'never',
        },
        {
          'if': ruleProtected + ruleAnd(ruleOnDefault) + ruleAnd(ruleNotSchedule),
          changes: [
            // Chart YAML
            '%s/Chart.yaml' % dir,
          ],
        },
      ],
    }
    for dir in std.split(std.extVar('dirs'), ',')
  };

local helmDocs = {
  'helm::docs': {
    extends: ['.k8s-workloads-image', '.id_tokens'],
    stage: 'docs',
    tags: [
      'gitlab-org',
    ],
    allow_failure: true,
    variables: {
      CHART_DIR: chartDir,
      CLONE_DIR: 'ci-helm-docs',
    },
    secrets: {
      HELM_DOCS_TOKEN: {
        vault: 'access_tokens/${VAULT_SECRETS_PATH}/helm-docs/token@ci',
        file: false,
      },
    },
    before_script: |||
      echo "Cloning ${CI_PROJECT_URL} into ${CLONE_DIR}"
      git config --global user.email "${GITLAB_USER_EMAIL}"
      git config --global user.name "${GITLAB_USER_NAME}"
      git clone ${CI_SERVER_PROTOCOL}://gitlab-ci-token:${HELM_DOCS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git ${CLONE_DIR}
    |||,
    script: |||
      cd ${CLONE_DIR}
      helm-docs --template-files=../.helmdocs.gotmpl -c ${CHART_DIR}
      git add .
      git commit -a -m "[ci skip]chore: updates README.md for charts - auto generated by helm-docs." || (echo "No change to add" && exit 0)
      git push ${CI_SERVER_PROTOCOL}://gitlab-ci-token:${HELM_DOCS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git ${CI_DEFAULT_BRANCH}
    |||,
    rules: [
      {
        'if': ruleGitlabOps,
        when: 'never',
      },
      {
        'if': ruleProtected + ruleAnd(ruleOnDefault) + ruleAnd(ruleNotSchedule),
        changes: [
          '%s/**' % chartDir,
          '.helmdocs.gotmpl',
        ],
      },
    ],
  },
};

local notifyMR = {
  'notify-mr': {
    extends: ['.k8s-workloads-image', '.id_tokens'],
    tags: [
      'gitlab-org',
    ],
    stage: 'notify',
    secrets: {
      GITLAB_API_TOKEN: {
        vault: 'access_tokens/gitlab-com/${CI_PROJECT_PATH}/notify-mr/token@ci',
        file: false,
      },
    },
    script: [
      '/k8s-workloads/notify-mr -s',
    ],
    allow_failure: true,
    rules: [
      {
        'if': ruleGitlabOps,
        when: 'always',
      },
    ],
  },
};

local gitlabCIConf =
  stages
  + includes
  + globalVars
  + idTokens
  + ciDiff
  + helm
  + helmJobs()
  + helmDocs
  + notifyMR;

// Render
std.manifestYamlDoc(gitlabCIConf)
