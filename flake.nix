{
  description = "Gitlab Charts";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = {self, nixpkgs, flake-utils, ...}: 
    flake-utils.lib.eachDefaultSystem (system:
      let 
        pkgs = import nixpkgs { 
          inherit system; 
          
          config.allowUnfree = true;
        };
        
      in {
        packages = {};
        devShell = (pkgs.callPackage ./shell.nix {
          mkShell   = pkgs.mkShell;
          helm      = pkgs.kubernetes-helm;
          jsonnet   = pkgs.jsonnet;
          coreutils = pkgs.coreutils-full;
          yq        = pkgs.yq-go;
          jb        = pkgs.jsonnet-bundler;
          make      = pkgs.gnumake;
        });
      });
}