# Gitlab Infrastructure Charts

Helm charts leveraged by the Gitlab infrastructure team.  

## Using the repo

Charts are pushed via `CI/CD` using the native OCI registry as well as GitLab package registry (Helm).  
This repo is mirrored to https://ops.gitlab.net/gitlab-com/gl-infra/charts, and the push operation happens on this instance.  
You can add the registry using:

```bash
# OCI 
helm registry login registry.ops.gitlab.net

# GitLab package registry (Helm)
helm repo add --username <username> --password <access_token> gl-infra-charts https://ops.gitlab.net/api/v4/projects/647/packages/helm/stable
```

## Adding a chart 

Ensure you read the [contributing guidelines](./CONTRIBUTING.md).  

Charts exist under `gitlab/` and should be added here.  

Example adding a new chart: 
```bash
# create new chart or add existing
helm create ./gitlab/mychart

# generate CI for the new chart
make generate
```

## Updating CI

CI is generated via `jsonnet`.  
When adding a new chart you must run `make generate`.  
This will automatically update the `.gitlab-ci.yml` which you should commit with your change.  
If you don't do this the CI will fail.  

## Documentation

We leverage [helm-docs](https://github.com/norwoodj/helm-docs) to autogenerate a `README.md` via CI per chart dir.  
Ensure you comment your `values.yaml` files appropriately.  
Each configurable setting should be a comment prefixed with `# --` to explain what the option does.  
We use a custom template that also supports `@raw` generation of markdown html.  
This is useful when a default value might be an empty map or list and you want to provide an example.  

```yaml
# --  Array containing Secret Stores to create<br>
# @raw
# ref: https://external-secrets.io/v0.5.8/api-secretstore/
# <br><br>
# <details>
# <summary>Example</summary>
#
# ```yaml
# secretStores:
#   - name: mysecretstore
#     authMountPath: kubernetes/mycluster
#     role: my-app
#     path: k8skv
#     version: v2
#     server: https://vault.domain
# ```
#
# </details>
secretStores: []
```

You can read me via the [official docs](https://github.com/norwoodj/helm-docs#helm-docs).  