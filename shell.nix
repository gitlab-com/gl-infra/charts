# shell.nix
{ 
  mkShell,
  helm,
  jsonnet,
  coreutils,
  yq,
  jb,
  make
}:
let

in mkShell {
  name = "GL-Infra/Charts";
  packages = [
    helm
    jsonnet
    coreutils
    yq
    jb
    make
  ];
  shellHook = ''''; 
} 
